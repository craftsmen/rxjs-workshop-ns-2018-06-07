
import { number$ } from '../lib/example-streams';

// The number$ stream (imported above) will emit short a sequence of numbers with an interval of 0.5 seconds.
//
// ASSIGNMENT: Subscribe to the number$ stream and print each number to the console.

// number$.???
